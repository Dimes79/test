## Bot API

***

###Namespace
```
- App\Api
- App\Api\Models (модели)
```

***

### Константы
```php
USER_CANT_REPORT - пользователь не может отправлять отчеты
HOURS_LIMIT      - превышен суточный лимит для отчетов
```

***

### Модели
- [User](#markdown-header-user) Пользователь
- [Project](#markdown-header-project)  Проект
- [Report](#markdown-header-report) Отчет


#### Project
```php
 Project {
     (int)id
     (int)name
 }
```

#### User
```php
 User {
    (string)id
    (string)userName - Nickname в телеграме
    (string)name
    (int)isReporter - может ли пользователь отправлять отчеты
    (int)approved - может пользоваться ботом
    (Report[])$reports -  список очетов
 }
```

#### Report
Отчеты пользователей
```php
 Report {
     (int)projectID
     (int)$projectName - название проекта
     (int)userID
     (int)hours - количество списанных часов
     (string)description - описание оставленное пользователем
 }
```

### Инициализация
```php
// $companyID - ID компании
$api = new BotApi( (int)$companyID );
```

***

### Методы
- [newUser](#markdown-header-newuser) добавление нового пользователя
- [getUser](#markdown-header-getuser) получение профайла пользователя
- [updateUser](#markdown-header-updateuser) обновление профайла пользователя
- [getLazyUsers](#markdown-header-getlazyusers) получение списка пользователей списавшие не все часы за сегодня
- [getUserLazyDays](#markdown-header-getuserlazydays) получение списка пользователей списавшие не все часы в указанный интервал
- [getUserReportToday](#markdown-header-getuserreporttoday) получение списка отчетов пользователя за сегодня
- [getProjects](#markdown-header-getprojects) получение списка проектов
- [getProjectByName](#markdown-header-getprojectbyname) ищем проект по имени
- [getTextes](#markdown-header-gettextes) - получение списка текстовок для бота
- [newReport](#markdown-header-newreport) - новый отчет от пользователя
- [newHoliday](#markdown-header-newholiday) - пользователь отмечает свой отпуск(отгул)
- [newSick](#markdown-header-newsick) - пользователь отмечает свой больничный

#### newUser
добавление нового пользователя
```php
$api->newUser(User $user): bool;
```

#### getUser
получение профайла пользователя
```php
$api->getUser(int $userID): User | null;
```

#### updateUser
обновление профайла пользователя

обновляются поля:

- userName
- name
```php
$api->updateUser(User $user): bool
```

#### getLazyUsers
получение списка пользователей списавшие не все часы за сегодня

- если сегодня выходной то список всегда пустой
```php
$api->getLazyUsers(): User[];
```

#### getUserLazyDays
получение списка пользователей списавшие не все часы в указанный интервал

- формат дат: YYYY-MM-DD
- если не указан $dateTo то выдает только за дату $dateFrom

```php
$api->getUserLazyDays(int $userID, string $dateFrom, string $dateTo = null): Object;
```
Результат выполнения:
```php
Object {
    (int)maxHours - сколько в день должен списывать пользователь
    (int)totalLeftHours - сколько всего часов не списано пользователем
    dates-> [
        Object {
            (string)date - YYYY-MM-DD
            (int)hours - сколько всего списано
            (int)leftHours - сколько нужно еще списать
        }, ...
    ]
}
```

#### getUserReportToday
получение списка отчетов пользователя за сегодня

```php
$api->getUserReportToday(int $userID): Report[];
```

#### getProjects
получение списка проектов

```php
$api->getProjects(): Project[];
```

#### getProjectByName
ищем проект по имени

```php
$api->getProjectByName(string $name): Project;
```

#### getTextes
получение списка текстовок для бота

```php
$api->getTextes(): [];
```

Результат выполнения:
```php
 [
   (string)key => (string)text,
   ...
 ]
```

#### newReport
новый отчет от пользователя

```php
$api->newReport(Report $report): bool | USER_CANT_REPORT | HOURS_LIMIT;
```

#### newHoliday
пользователь отмечает свой отпуск(отгул)

- формат дат YYYY-MM-DD
- если не указан $dateTo отмечает только один день $dateFrom
```php
$api->newHoliday(int $userID, string $dateFrom, string $dateTo = null): bool | USER_CANT_REPORT;
```

#### newSick
пользователь отмечает свой больничный

- формат дат YYYY-MM-DD
- если не указан $dateTo отмечает только один день $dateFrom
```php
$api->newSick(int $userID, int $hours, string $dateFrom, string $dateTo = null): bool | USER_CANT_REPORT;
```