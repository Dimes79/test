## Front-end API

***

### Сотрудники компании
- [GET /api/users](#markdown-header-get-apiusers) Получени списка сотрудников
- [GET /api/users/names](#markdown-header-get-apiusersnames) Получени списка имен пользователей
- [POST /api/users](#markdown-header-post-apiusersnames) Добавление нового пользователя